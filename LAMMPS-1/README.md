# Using LAMMPS to simulate water in a box



## About LAMMPS

LAMMPS is an open-source software (under GNU General Public License) maintained and distributed by Sandia National Lab and Temple University. Anyone can download and use this software for free. See its [website](https://www.lammps.org/) and it has a terrific [manual](https://docs.lammps.org/) to teach you from the begining. As long as one has very basic knowledge of chemistry and physics, you will know how to use this software. Of course, some efforts are needed to get familiar with it. This is copied from their website about its functions:

"LAMMPS is a classical molecular dynamics (MD) code that models ensembles of particles in a liquid, solid, or gaseous state. It can model atomic, polymeric, biological, solid-state (metals, ceramics, oxides), granular, coarse-grained, or macroscopic systems using a variety of interatomic potentials (force fields) and boundary conditions. It can model 2d or 3d systems with only a few particles up to millions or billions."

In this tutorial, we will learn how to simulate a bunch of water molecules in a large box at a constant temperature or pressure. 


## Input parameters

Following is an example of simulating 600 atom water (200 water molecules) system.

![An illustration for this water model](600-atom-water.png)


LAMMPS needs two types of files for a basic classical MD simulation. 1. A parameter file (with whatever file name you want) containing all the parameters, commands, and flags and 2. a data file (also you can use any file name) describing the system e.g. atomic position, atomic species, bonds, angles, etc. Let's check out the parameter file:

```
units         real               # define the unit of the system used
dimension     3                  # we are simulating a 3 dimensional system
boundary      p p p              # this system is periodic in all x, y, and z direction
atom_style    full               # a general description of the system
read_data     600water.dat       # which file to read the description of the system (program will look for 600water.dat when starting)

# create groups #
group ox type 1                  # call the type 1 atom (defined in data file 600water.dat) as "ox"
group hy type 2                  # call the type 2 atom (defined in data file 600water.dat) as "hy"

# set charges - beside manually #
set group ox charge -0.830       # manually force the charge of oxygen to be -0.83
set group hy charge 0.415        # manually force the charge of hydrogen to be 0.415
                                 # such enforcement is because different FF has different charge, by
                                 #   this command, the data file does not need to be changed every time
                                 #   if another FF is used (data file also has charge description)

### TIP3P Potential Parameters ###
bond_style      harmonic
bond_coeff      1 450 0.9572
angle_style     harmonic
angle_coeff     1 55 104.52
pair_style      lj/cut/coul/long 10.0
kspace_style    pppm 1.0e-5 #final npt relaxation
pair_coeff      1 1 0.102 3.188 
pair_coeff      2 1*2 0.000 0.000 

velocity all create 300.0 1234546 dist gaussian     # create the initial temperature for all atoms
fix 1 all nvt temp 300.0 300.0 400.0                # use nvt ensemble

neighbor 2.0 bin
neigh_modify delay 0 every 10 check yes            # for every 10 steps, re-check the neighbors (for pair interactions)
thermo      100                                    # for every 100 steps, print out info to the screen
thermo_style custom step etotal pe ke temp press   # print on the screen step_index, total energy, pot energy, kinetic energy, temperature, pressure
thermo_modify norm no flush yes

dump waterdump all atom 10000 300.dump             # for every 10000 steps, save to the file 300.dump
dump_modify waterdump sort id
dump_modify waterdump scale no
restart 10000 300K.restart                         # for every 10000 steps, save the calculation to 300K.restart
                                                   #   one can restart the calculation for this file
timestep 0.5
run 240000                                         # run 240000 steps (total time length will be 120ps)
```

Every line is a command with some parameters or flags after it. 

**units**: see this [explanation](https://docs.lammps.org/units.html) for the general unit usage. Find out what **real** unit style means e.g. for mass, length, time, energy, force, temperature, pressure, etc.

**atom_style**: see [atom_style](https://docs.lammps.org/atom_style.html) full = molecules + charge

**bond_style**: see [bond_style](https://docs.lammps.org/bond_style.html). The harmonic bond style is used. The following [**bond_coeff**](https://docs.lammps.org/bond_coeff.html) describes the coefficients used for the harmonic bonds. For the following parameters: '1' means for bond type 1 (bond type is defined in 600water.dat), the equilibrium bond length is 0.9572 Angstrom, and the coefficient k<sub>b</sub> is 450 kcal/(mole\*Angstrom<sup>2</sup>). For this system, there is only **one** bond type, i.e. H-O bond. But for other complicated systems, there could be multiple bond types. For those cases, we can define bond type 2, 3 or more and the bond types are defined in data file (600water.dat):

```
bond_coeff    1  450 0.9572
bond_coeff    2   50 2.20
bond_coeff    3  150 1.3544
```

**angle_style**: see [angle_style](https://docs.lammps.org/angle_style.html). Similar to bond_style.

**pair_style**: the above bond and angle styles describes *intra-molecular* interactions. For *inter-molecular* interactions (such as vdW), **pair_style** is used to describe them. LAMMPS has a very complicated and massive pair-interaction ensemble (see [pair_style](https://docs.lammps.org/pairs.html)). For water tip3p or tip4p force field (FF), the l-j FF is usually used. Here, we use `lj/cut/coul/long 10.0` (see its [explanation](https://docs.lammps.org/pair_lj_cut_coul.html#pair-style-lj-cut-coul-long-command)). The number '10.0' indicates that the real space Coulomb interaction has a distance cutoff as 10 Angstrom. 

**kspace_style**: remember Ewald summation. 'lj/cut/coul/long' describes summing over real space, kspace_style describes k-space summation.

**pair_coeff**: once the pair interaction style is determined, their coefficients are listed here. `pair_coeff  1 1 0.102 3.188` indicates the lj potential parameters between type 1 atoms and type 1 atoms (oxygen-oxygen ineraction). `pair_coeff  2 1*2 0.0 0.0` means for any interactions between type 2 and type 1 OR type2 and type 2 atoms are set as zero (no interactions). 

**:warning:** pair_coeff or pair_style only describes interactions of atoms belonging to **different** molecules (molecules are defined in data file 600water.dat). There is no pair interaction for atoms within one molecule. 

**velocity**: this command is used here to give the initial velocity to all atoms. The created temperature is 300K with random seed 1234546. They also have a gaussian distribution. 

**fix**: [fix](https://docs.lammps.org/fix.html) is another complicated command which generally give any constraints, modifications, extra forces/velocities, etc during the simulation. Here, we use fix command to specify the ensemble. '1' means the first fix command style (since there may be multiple fix command); 'all' means all atoms; 'nvt' is the ensemble used here; '300.0 300.0 400.0' describes NVT ensemble temperature from 300K to 300K with damping 400 fs (400 fs is used for Nose-Hoover thermostat). 

**thermo_style**: [thermo_syle](https://docs.lammps.org/thermo_style.html) cutomizes what information printed on the screen and how frequent it is printed (or in log.lammps file).

**dump**: save the coordinates or anything else you defined to a file. Here, we save atom positions ('atom' flag). This is also called the trajectory file.

**restart**: for every 10000 steps, save the calculation to a file (300K.restart). The calculation can be resumed by reading 300K.restart. When restarting, put a line `read_restart 298K.restart.xxxx` before the line `read_data xxxx`.

**timestep**: setup the time step.

**run**: start to run with above defined calculation.


## Data file

Now, let's look at the data file which defines the system.

```
LAMMPS DATA FILE

     600   atoms
     200   angles
     400   bonds

     2    atom types
     1    bond types
     1   angle types

     0.0000000000      18.1707000000    xlo xhi
     0.0000000000      18.1707000000    ylo yhi
     0.0000000000      18.1707000000    zlo zhi

Masses

  1   15.9994
  2    1.008 

Atoms

1     1     1      -0.830          17.5534     7.92771     0.768872
2     1     2       0.415          17.2469     8.84996     0.891728
3     1     2       0.415          17.1703     7.50022     1.56897
4     2     1      -0.830          4.15011     4.43985     5.32726
5     2     2       0.415          3.52688     5.17029     5.11515
6     2     2       0.415          4.56703     4.79923     6.1429
... ...

Bonds

   1    1    1   2
   2    1    1   3
   3    1    4   5
   4    1    4   6
   5    1    7   8
   ... ... 

Angles

   1    1    2   1  3
   2    1    5   4  6
   3    1    8   7  9
   4    1    11   10  12
   5    1    14   13  15
   ... ... 
```

For water system, the data file has five blocks (see [here](https://docs.lammps.org/read_data.html) for formats of a data file):

**Header**: it shows a general info for the system (such as number of atoms, bonds, angles, etc). It also indicates how many types of bonds, angle, and atoms. The box is also defined here (we have a cubic box with 18.1707 Angstrom length).

**Masses**: type 1 atom (oxygen) has mass 15.9994; type 2 atom (hydrogen) has mass 1.008.

**Atoms** block: 

- first column: atom index
- second column: molecule index
- third column: atom types
- forth column: charge for each atom
- five to seven columns: x, y, z position for each atom

**Bonds** block: 

- first column: bond index
- second column: bond type (here, there is only one bond type: O-H bond)
- third and forth column: atom indexes for this bond (e.g. atom 7 and atom 8 form bond 5)

**Angle** block: this is similar to bonds block except that there are three atoms associated with an angle (for bond, only two atoms are needed).


## Submit the job

This is the contents of job submitting script:

```
#!/bin/bash

#PBS  -N   test
#PBS  -l   nodes=1:ppn=2                # use one node with 2 processors
#PBS  -l   walltime=1:30:0              # walltime of the job is set as 30 min
#PBS  -S   /bin/bash
#PBS  -j   oe 
#PBS  -q   pub_jx

cd $PBS_O_WORKDIR

NPROC=`wc -l < $PBS_NODEFILE`

echo This job has allocated $NPROC nodes > log

module load apps/lammps/intelmpi/29Oct20

mpirun  -np $NPROC  lmp_intelmpi -in water-tip3p.lmp >& water-tip3p.out
```

Now run `qsub submit.pbs`. `qstat` to check the status of your job.

This is an example `qstat` output on the screen:

```
node1: 
                                                                                  Req'd    Req'd       Elap
Job ID                  Username    Queue    Jobname          SessID  NDS   TSK   Memory   Time    S   Time
----------------------- ----------- -------- ---------------- ------ ----- ------ ------ --------- - ---------
3163588.node1           username    pub_jx   hw5                 --      1     2     --   00:15:00 Q       -- 
```

For column 'S', 'Q' indicates still waiting in the queue; 'R' means running; 'C' means finished or error. If nothing shows after `qstat` command, it usually means your job is finished or killed. 

For LAMMPS, start the calculation with the command `lmp_intelmpi -in input >& output`. When it is finished, you will see some new files in your directory: 

- log.lammps is exactly the same to the information printed on the screen when you run the calculation

- 300K.dump saves the trajectory for further trajectory analysis

- 300K.restart.x0000 saves the information for restarting


## RDF computation

```python

def Lorentizan_broadening(y0, x0, nbins, broadwidth, dx):
    eindex = int( np.rint(x0/dx) )
    lbroaden = np.zeros(nbins)
    resolution=10
    nwidth = int(resolution*broadwidth/dx)
    for i in range( max(eindex-nwidth,0), min(eindex+nwidth,nbins) ):
      lbroaden[i]=y0*broadwidth/(2.0*np.pi*((float(eindex-i))**2+0.25*(broadwidth/dx)**2)*dx**2)
    return lbroaden


def Gaussian_broadening(y0, x0, nbins, broadwidth, dx):
    eindex = int( np.rint(x0/dx) )
    lbroaden = np.zeros(nbins)
    resolution=10
    nwidth = int(resolution*broadwidth/dx)
    for i in range( max(eindex-nwidth,0), min(eindex+nwidth,nbins) ):
      lbroaden[i]=y0/(2.0*np.pi*broadwidth**2)**0.5*np.exp( -float(i-eindex)**2/(2.0*broadwidth/dx) )
    return lbroaden


def compute_distance_between_A_and_B( Lat, CoordA, CoordB ):
    Rij = np.zeros( 27 )
    i = 0
    for x in [-1, 0, 1]:
      for y in [-1, 0, 1]:
        for z in [-1, 0, 1]:
            # shift Coord B in 27 directions
            Rij[i] = np.linalg.norm( CoordB + ... - CoordA )
    return np.min( Rij )


def comp_RDF_B_around_A( Coord, Name, Lat, cutoff, Species_A, NAtom_A, Species_B, NAtom_B, nbins, broadwidth ):
    
    rgrid = np.linspace(0.0, cutoff, num=nbins, ... )
    dx = rgrid[1] - rgrid[0]

    gA_B_ave = np.zeros( nbins )

    # Name contains the species of all atoms (in a 1D array)
    for i, ia in enumerate( Name ):

      if ia == Species_A:

        for j, jb in enumerate( Name ):
    
          if jb == Species_B:

            Dist = ...  # use A, B coordinates, write function to compute Dist (consider PBC)

            if Dist <= cutoff and Dist > 0:
              y0 = 1.0/Dist**2
              gA_B_ave = gA_B + Gaussian_broadening( y0, Dist, nbins, ...  )

    gA_B_ave = gA_B_ave / NAtom_A

    return rgrid, gA_B_ave

# reading coordinates
Coords_all_steps = ...

# set up parameters
nstep = ...
nbins = ...
cutoff = 8.0 # set it to be 8.0 Angstrom
NAtom_A = ...
NAtom_B = ...
Species_A = 1
Species_B = 2
nbins =  ...
broadwidth = ... # ~ 1Angstrom, you can tune this number to make the final graph smooth
g_r_ave = np.zeros( nbins )

# since all time steps have same Name and Lat, you can take the first one
# Name contains the species of each atoms, the order of Name should be 
#    consistent to the order of the corresponding coordinates
#    e.g. Name could be [1, 2, 2, 1, 2, 2, 1, 2, 2, ...]
Name = ...
Lat = ...

# compute RDF
for istep in range(0, nstep):
    rgrid, g_r = comp_B_around_A_RDF( Coords_all_steps[istep], Name, Lat, cutoff, ... )
    g_r_ave = g_r_ave + g_r/nstep

# plot rgrid and g_r_ave

```

## HW

:pushpin: Set up a NVT MD simulation, set the final equilibrium temperature to be 330K and run for 100ps. Use the information in log.lammps, plot the kinetic energy, temeprature, potential energy, and total energy as a function of time.

:pushpin: Find out the time range in which you think the MD simulation reaches equilibrium by visulizing plots from Question1.

:pushpin: Use the trajectory within the **equilibrium** period to compute the time-averaged RDF for O-O and compare with the experiments. (You may need to use the script (download [here](extract.py)) to read dump file and also remember to cite the source of the experimental data.) Here, let's average the RDF every 10000 steps. For 120ps run, around 20 structures will be used for average.

:pushpin: Instead of NVT ensemble, use [NPT](https://docs.lammps.org/fix_nh.html) to redo the simulation. Fix the pressure of the system at 1 atmosphere and temperature at 330K. Plot how the volume and temperature of the system change as a function of time and try to simulate until the system equilibrated. (**Note** that for NPT, we will monitor both temperature and volume to justify if the system is in equilibrium, i.e. both temperature and volume are fluctuating around a specific value.)

