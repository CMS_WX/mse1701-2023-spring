DFT calculation on a surface
=========================================

In this tutorial, we will use QE to study adsorption of a molecule (CO) on metal surfaces and related chemical reaction simulations.

## Slab model

Surfaces or interfaces do not have translational symmetry for the direction normal to the surface (or interface). To model such 2D-type system, a **large enough vacuum space** is usually used, i.e. in out-of-plane direction of the 2D slab, there is vacuum to vanish the slab-slab interaction to mimic the broken translational symmetry. In-plane directions of the 2D slab is still similar to bulk case without any vacuum. Usually, the size of the vacuum (more specifically its height) needs a convergence test. However, slab calculation is usually expensive owing to the vacuum space. We may want to have a balance between computational accuracy and cost. 

Similarly, for 1D system: vacuum in x-y directions, but bulk-like along z direction. For 0D system, such as simulating a molecule, simply put this molecule in a large enough box with vacuum in all x, y and z directions of this molecule to avoid image-image ineractions. 

<img src="surf1.png" width="70%" height="70%">

The above figure display a **4-layer Cu slab** model. The vacuum height is around **15 Angstrom** from top layer Cu to bottom Cu layer of the image above. What the figure shows is a **(111) direction** surface. Which surface should be studied is a science question, depending on if this surface is stable and if it is interesting enough for investigation. Cu (111) is famous for CO2 reduction. In this example, we will study CO on Cu surface, which is one of the most important steps of CO2 reduction. Meanwhile, number of metal layers may also need convergence test. For the aim of illustration for this course, we use 3 layers for now. 3 layers metal may **NOT** be enough for most catalytic studies, remember to check its convergence.


## CO adsorption energy

The molecular **adsorption energy** $`E_{\rm ads}`$ is computed as:

```math
    E_{\rm ads}({\rm CO}) = E_{\rm tot}({\rm CO-Cu_surf}) - E_{\rm tot}({\rm CO}) - E_{\rm tot}({\rm Cu_surf})
```

i.e. it is the energy difference between DFT computed total energy of combined system CO on Cu and the total energies of pure CO plus the pure Cu surface. Every system in the above equation needs a relaxation, since when CO is adsorbed on Cu surface, both CO and a few atoms of Cu near the adsorption site could change their structures slightly. 

Following is the input for Cu surface relaxation (in\_surf):

    &control                         
       calculation = 'relax' 
       restart_mode='from_scratch',     
       prefix='Cu111',                
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       forc_conv_thr = 0.001d0,   
       nstep = 2000,              
       outdir='./'                      
    /
    &system
       ibrav=  0,                    
       nat=  27,
       ntyp= 1,                      
       ecutwfc =50.0,
       nspin = 1,                    
       occupations = 'smearing',        
       smearing = 'm-v',
       degauss = 0.002,
       input_dft = 'rpbe',
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-7
    /
    
    &ions
    
    /
    
    ATOMIC_SPECIES
      Cu   28.08  Cu.SG15.PBE.UPF
    
    K_POINTS {automatic}             
     2  2  1  0  0  0
    
    CELL_PARAMETERS {Angstrom}
      7.92537000  0.00000000  0.00000000
      -3.96269000  6.86357000  0.00000000
      0.00000000  0.00000000  20.74720000
    
    ATOMIC_POSITIONS (crystal)
      Cu    0.108574    0.221000    0.105099    0    0    0
      Cu    0.111990    0.890635    0.105344    0    0    0
      Cu    0.225282    0.777856    0.195200    1    1    1
      Cu    0.888915    0.111083    0.195603    1    1    1
      ... ...
      ... ...

- integers after atomic coordinates (0  0  0 or 1  1  1) specify if this atom will be allowed to move (along x, y, z) during relaxation. 0: do not move; 1: move. For slab systems, to avoid the whole structure shifting along z, the few bottom layers are to be fixed during the relaxation (or MD simulation). Here, we fix the bottom layer of Cu. 

- For k point sampling, usually a single k point is sampled along **vacuum direction** (i.e. non-periodic direction). 

- Different from the semiconductors we have practices before, a smearing method is usually used for metal to help convergence.

- Meanwhile, it is found that PBE cannot predict correct CO adsorption site. Throughout this practice, we will use 'rPBE' see [ref](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.59.7413). In real CO2 reduction reactions (CO2RR), solvent is actually quite important (water molecules and cations disolved in water). For this practice, we will only do the calculation in vacuum as an illustration purpose. 

Here, the adsorption site of CO on Cu was in debate for a long time. Then some experiments have shown that CO tend to adsorb on top of Cu atoms (at least gas phase expt.). But if there is no expt. suggestions, we may have to examing all possible sites for adsorption and compare which site provides the most stable adsorption (which has the strongest adsorption energy). 

:pushpin: Relax and compute the total energy of the pure Cu(111) surface (without CO adsorption).

:pushpin: Relax and compute the total energy of the CO molecule in a big box (such as $`15\times15\times15`$ Angstrom box, remember we only need single k-point sampling for 0D system).

:pushpin: We put CO on top of one of Cu atoms on the surface to build up an adsorption system (carbon is attached to Cu atom, oxygen is on top of carbon, you can put CO on top of any top-layer Cu atoms). Then we relax the whole system (fix bottom layer) using in\_CO-on-surf and compute the adsorption energy. The following figure is how it looks from top and side view. Let's build the initial structure by putting C around 1.8 Angstrom on top of Cu atom. (for topview, only the first layer Cu atoms is shown)

<img src="top.png" width="50%" height="50%">

:pushpin: Now let's change the adsorption site to the hollow site of Cu(111) surface (similar to top-site the C is attached to Cu, you can put CO on top of any hollow sites of top-layer Cu), relax the system and compute the adsorption energy. Similarly, when building the hollow site initial structure, one can put the CO (vertically) on top of three-Cu-atom hollow center roughly round 1.8 Angstrom.  (for topview, only the first layer Cu atoms is shown)

<img src="hollow.png" width="50%" height="50%">

:pushpin: Finally, let's change the adsorption site to the bridge site of Cu(111) surface (i.e. the C is on top of the center of two-neighboring-Cu atoms), relax the system and compute the adsorption energy. (for topview, only the first layer Cu atoms is shown)

<img src="bridge.png" width="50%" height="50%">

Make sure that after the relaxation if the final relaxed structure still presents the so-called "top", "hollow", and "bridge" adsorption. Note that when computing the adsorption energy, some quantities can be shared: such as the total energies of the pure Cu(111) surface and the CO molecule in a big box. All the above calculations are much more expensive than the bulk cases we have tried before. Be patient! After the relaxation, extracting the total energy for the relaxed systems and find out the adsorption energy! 

Some flaws of the calculation here:

* In this practice, we only use the DFT calculated total energy (i.e. 0K energy), which does not contain the contributions from the entropy and the finite temperature. A good calculation should use the finite temperature **free energies**.

* Owing to computational cost, only three layers of Cu are used here, which is a big issue in this calculation. The number of metal layers could strongly affect adsorption energy either for physical or chemical adsorptions. 

* The physical adsorption structure is not fully explored. Intuitively, CO molecule with a lie-down position may be a more stable physical adsorption structure. Also, we didn't check the Oxygen end attached to Cu atoms. Usually, this configuration is more energy costive.

* CO has a dipole from C to O. For DFT surface/slab calculations with periodic images in the vacuum direction, such dipole is artificial and it is necessary to eliminate the dipole by dipole-correction technique. 
