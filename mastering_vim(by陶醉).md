# Syntax of the Language

## Verb + Noun

**d** for delete

**w** for word

combine to be "delete word"

## Commands are Repeatable & Undoable

（命令可重复可撤销）

If we do a command, use "." to **repeat** this command.

Use u to **Undo** a command.

## Verbs in Vim

The operation you want to take on the text

- d => **Delete**
- c => **Change **(delete and enter insert mode)
- \> => **Indent **(缩进)
- v => **Visual select**
- y => **Yank**(copy)
- . => **Do the last commands you do**
- w => **Save**
- q => **Quit** 
- x => **Delete and Return**

## Nouns in Vim -- Motions

- w => **word **(forward by a "word")
- b => **back** (back by a "word")
- 2j => down 2 lines
- n => next 
- N => last

## Nouns in Vim -- Text Objects

- iw => "inner word" (works from anywhere in a word)
- it => "inner tag"(the contents of an HTML tag)
- i" => "inner quotes"
- ip => "inner paragraph"
- as => "a sentence"

**e.g.**

- diw => delete the word I am on, no matter on the beginning of the word or the middle of the word.

- di"

- di)

- di]

- dip

- das

## Nouns in Vim -- Parameterized Text Objects

Search a sentence:

- f,F => "find" the next character  (change cursor right on the character)
  - Capital F means searching back, f means searching forward
- t,T => "find" the next character  (change cursor right before the character)

Search the whole paragraph:

- / => Search (up to the next match)

**e.g.**

- ctL delete and change the content between cursor and L

  - <p>^User List</p>   ctL  =>   <p>^List<p>

## Combinatorics of Commands

5 operators * 10 motions

\+ 5 operators * 10 text objects

\+ 5 operators * 35 characters (all the lower and upper case letters/ punctuations/ numbers...) * 4 (for 'f','F','t','T')

\+ 5 operators * ~100 (for '/')

= **2000**

Distinct commands based on memorizing ~30 key mappings(that are very memorable)



## Tips for Mastering the Language

The "dot" command

- Use the more general text object (iw rather than w even if at beginning of word)
- Prefer text objects to motions when possible
- Repeat.vim for plugin repeating

​												



# Relative Number

- :set relativenumber

​		:set number

- :set nornu

## Visual Mode Is a Smell

Don't use two sentences where one will do

Breaks repeatability

## Custom Operators

- Surround

  - delete change and add surrounding things

    - e.g. ds"(delete") ;  cs"'  (change " to '')

  - add surrounding things

    - e.g. ysiw" (surround the inner word with double quote")

  - this also works with tags

    - e.g. cs<h2<h2>

  - Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, and more. The plugin provides mappings to easily delete, change and add such surroundings in pairs.

    It's easiest to explain with examples. Press `cs"'` inside

    ```
    "Hello world!"
    ```

    to change it to

    ```
    'Hello world!'
    ```

    Now press `cs'<q>` to change it to

    ```
    <q>Hello world!</q>
    ```

    To go full circle, press `cst"` to get

    ```
    "Hello world!"
    ```

    To remove the delimiters entirely, press `ds"`.

    ```
    Hello world!
    ```

    Now with the cursor on "Hello", press `ysiw]` (`iw` is a text object).

    ```
    [Hello] world!
    ```

    Let's make that braces and add some space (use `}` instead of `{` for no space): `cs]{`

    ```
    { Hello } world!
    ```

    Now wrap the entire line in parentheses with `yssb` or `yss)`.

    ```
    ({ Hello } world!)
    ```

    Revert to the original text: `ds{ds)`

    ```
    Hello world!
    ```

    Emphasize hello: `ysiw<em>`

    ```
    <em>Hello</em> world!
    ```

    Finally, let's try out visual mode. Press a capital V (for linewise visual mode) followed by `S<p class="important">`.

    ```
    <p class="important">
      <em>Hello</em> world!
    </p>
    ```

    This plugin is very powerful for HTML and XML editing, a niche which currently seems underfilled in Vim land. (As opposed to HTML/XML *inserting*, for which many plugins are available). Adding, changing, and removing pairs of tags simultaneously is a breeze.

    The `.` command will work with `ds`, `cs`, and `yss` if you install [repeat.vim](https://github.com/tpope/vim-repeat).

- Commentary

  - Comment stuff out. Use `gcc` to comment out a line (takes a count), `gc` to comment out the target of a motion (for example, `gcap` to comment out a paragraph), `gc` in visual mode to comment out the selection, and `gc` in operator pending mode to target a comment. You can also use it as a command, either with a range like `:7,17Commentary`, or as part of a `:global` invocation like with `:g/TODO/Commentary`. That's it.  （gc6j...)

    I wrote this because 5 years after Vim added support for mapping an operator, I still couldn't find a commenting plugin that leveraged that feature (I overlooked [tcomment.vim](https://github.com/tomtom/tcomment_vim)). Striving for minimalism, it weighs in at under 100 lines of code.

    Oh, and it uncomments, too. The above maps actually toggle, and `gcgc` uncomments a set of adjacent commented lines.

- ReplaceWithRegister

  - /s/.../...
  - /%s/.../...

- Titlecase

## Custom Text Objects

- Indent

  - operator + ii  edit an inner content
    - gcii
    - cii

- Entire

  - operator + ae
    - gcae
    - cae

- Line

- Ruby block

  - operator + ip

  

# reference

[Home · kana/vim-textobj-user Wiki (github.com)](https://github.com/kana/vim-textobj-user/wiki)

[ Mastering the Vim Language - YouTube](https://www.youtube.com/watch?v=wlR5gYd6um0)

  

