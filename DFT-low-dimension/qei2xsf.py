
import sys
import re
import numpy as np


###read_files(filename):
def read_files():
    infile = open(sys.argv[1], "r")
    raweig = infile.read()
    infile.close()
    return raweig
 
 
####find input
def find_input(qefile_in):

    ibrav = find_ibrav(qefile_in)

    alat = 1.0

    cell = find_cell( qefile_in )

    name, position, fix, unit = find_coord(qefile_in)

    if   ibrav==0:
         angle=1.0
         cell_parameter = cell
         ele_coord=position

    else:
         print ('please set ibrav to 0')
         sys.exit()

    if len(name) != ele_coord.shape[0]:
       print ('!ERROR!', len(name), ele_coord.shape[0] )
       print ('mismatch of names and coordinates' )
       sys.exit()

    return [ibrav, alat, angle, cell_parameter, ele_coord, name, unit]



###find ibrav
def find_ibrav(qefile_in):

    p=re.compile(r"ibrav\s*=\s*(\d)\,")
    ibrav = int(float(p.search(qefile_in).group(1)))

    return ibrav


###find alat
def find_alat(qefile_in):

    p=re.compile("celldm\(1\)\s*=\s*([\d\.\w]+),")
    alat = float((p.search(qefile_in).group(1).replace('d','e')))

    return alat


def find_cell( qefile_in ):
    p = re.compile('CELL_PARAMETERS\s*[\{\}\(\)a-zA-Z\s]+([\d\.\-Ee\+\s]*)')
    cell_raw = p.search( qefile_in ).group(1).strip('\n').split('\n')
    cell = [ [float(x) for x in cell_raw[0].split()], [float(x) for x in cell_raw[1].split()],[float(x) for x in cell_raw[2].split()] ]
    return np.array( cell )

###find coordinates for atoms
def find_coord(qefile_in):

    p=re.compile('ATOMIC_POSITIONS [\(\)a-zA-Z]*\s*([a-zA-Z\s\d\-\.]+)')
    q=re.compile('ATOMIC_POSITIONS \(([a-zA-Z]+)\)')
    coord_temp=p.findall(qefile_in)
    rawcoord = q.search(qefile_in).group(1)
    if rawcoord.lower() == 'angstrom':
        unit = 'angstrom'
    elif rawcoord.lower() == 'crystal':
        unit = 'crystal'
    else:
        print ('Unknow unit, use crystal or angstrom exit!')
        sys.exit()

    
    position = []; name = []; fix = []
    for icoord in coord_temp[0].strip('\n').split('\n'):
        line_tmp = icoord.split()
        name.append( line_tmp[0] )
        position.append( [float(x) for x in line_tmp[1:4]] )
        if len( line_tmp ) > 4:
            fix.append( [int(x) for x in line_tmp[4:7]] )
        else:
            fix.append( [0, 0, 0] )

    position = np.array( position )
    fix = np.array( fix )

    return name, position, fix, unit


###find elements names
def find_name(qefile_in):

    q=re.compile(r"\n([A-Z][a-z\s])[\s\-]*\d\.\d+",re.M)
    name=q.findall(qefile_in)

    return name

    
###calc CELL_PARAMETERS when ibrav=5; in units of Bohr
def ibrav_5(celldm_4):

    c_sq=2*1.0**2*(1-celldm_4)
    b_xy=3.0**0.5/3.0*c_sq**0.5
    b_z=(1.0**2-b_xy**2)**0.5

    cell=np.array( [[ b_xy/2.0*3.0**0.5,-b_xy/2.0, b_z],
                       [ 0                    , b_xy    , b_z],
                       [-b_xy/2.0*3.0**0.5,-b_xy/2.0, b_z]] )

    return cell



###find...
qeinput=[]
raweig=read_files()
qeinput=find_input(raweig)
alat=1.0

if qeinput[6] == 'crystal':
    coordinates=np.dot(qeinput[4],qeinput[3])
elif qeinput[6] == 'angstrom':
    coordinates=qeinput[4]

####print out
print (' DIM-GROUP')
print ('           3           1' )
print (' PRIMVEC' )
for ivector in range(0, 3):
    print ("  %11.7f   %11.7f   %11.7f" % (qeinput[3][ivector,0]*alat,qeinput[3][ivector,1]*alat,qeinput[3][ivector,2]*alat) )

print (' PRIMCOORD')
print ('          %d           1' %(qeinput[4].shape[0]))
for iposition in range(0,len(qeinput[5])):
    print ( " %s    %11.7f   %11.7f   %11.7f" % ( qeinput[5][iposition], coordinates[iposition,0],coordinates[iposition,1],coordinates[iposition,2]))
####
####
####

