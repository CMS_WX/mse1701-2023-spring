# Using LAMMPS to simulate solar cell materials


## Structure of CH<sub>3</sub>NH<sub>3</sub>PbI<sub>3</sub> (MAPbI<sub>3</sub>)

MAPbI<sub>3</sub> is a very promising solar cell. Meanwhile, its structure is also very interesting: it consists of organic molecules (MA) and an inorganic frame (PbI<sub>3</sub>). See the following figure for its structure: each Pb is bonding with six neighboring I atoms, and each I atom bridges two Pb atoms. Pb and I form an inorganic frame with MA molecules trapped in the frame. For this material, Pb has +2 charge, I has -1 charge, and the whole molecule is +1. More interestingly, the MA molecule is not symmetric, CH<sub>3</sub> end and NH<sub>3</sub> end are different with a dipole pointing from C to N. The research of dipole direction including the behavior of MA is always a focus for MAPbI<sub>3</sub> community. 

MAPbI<sub>3</sub> has three phases: orthorhombic (low temperature), tetragonal (around room temperature), and cubic (slightly above room temperature or higher). In this practice, we will learn how to build the input of LAMMPS for such a complicated structure and explore its physical properties.

![An illustration for this type of material](OMHP.png)

## LAMMPS input (parameter file)

See the following for the header of its data file. Here, the trick is that for MA, we treat it as a molecule as usual and use harmonic/amber/charmm to describe its bonds, angles, and dihedrals (amber or charmm is a special but widely used FF for organic or bio molecules). While for Pb and I atoms, instead of forming explicit bonds/angles, each atom is treated as an **atom**. Thus, there is no intra-molecular bonds (i.e. no explicit chemical bonds/angles); there is only pair interactions between Pb and Pb, Pb and I, and I and I. 

```
 Data file for lammps
 
        3072     atoms
        1792     bonds
        3072     angles
        2304     dihedrals
         0       impropers
 
           6 atom types
           7 bond types
          12 angle types
           9 dihedral types
 
 0   36.357600750765293            xlo xhi
 0   34.148161340100906            ylo yhi
 0   49.295601964453176            zlo zhi
 #xy xz yz   6.9643473961815304E-003   3.5430871207112885E-004  -1.0353292075508148E-004
 
 Masses
 
           1   207.19999999999999     
           2   126.90000000000001     
           3   1.0080000000000000     
           4   14.010000000000000     
           5   12.010000000000000     
           6   1.0080000000000000     
 
 Bond Coeffs
 
           1   338.69999999999999        1.0910000000000000     
           2   338.69999999999999        1.0910000000000000     
           3        seven types of bonds
           ... ...
 
 Angle Coeffs
 
           1   39.000000000000000        110.73999691837884     
           2   39.000000000000000        110.73999691837884     
           3       twelve types of angles
           ... ...
 
 Dihedral Coeffs
 
           1  0.15559999999999999                3           0   0.00000000    
           2  0.15559999999999999                3           0   0.00000000    
           3       nine types of dihedrals
           ... ...  
           ... ... ALL the above bonds/angles/dihedrals only describes MA, and they have nothing to do with PbI3
           ... ...  
 
 Atoms
 
           1           1           1   2.0299999999999998        2.6647002999999999E-002  -1.9386078000000008E-002  -3.1521480000000004E-003
           2           2           1   2.0299999999999998        4.5740350730000001        4.2606817570000004       -7.8453930000000026E-003
           3           3           1   2.0299999999999998        3.5756635000000002E-002  -4.6376400000000002E-003   6.1612644070000018     
           4           4           1   2.0299999999999998        4.5802336800000001        4.2693735860000004        6.1537896030000017     
           5           5           1   2.0299999999999998        9.1160470029999985       -2.0069078000000004E-002  -4.8683980000000012E-003
           6           6           1   2.0299999999999998        13.663435073000000        4.2599987570000000       -9.5616429999999999E-003
           7           7           1   2.0299999999999998        9.1251566349999980       -5.3206399999999989E-003   6.1595481570000006     
           ... ...
           ... ...   each Pb atom is a molecule
```

Let's have a view of its parameter file and look for pair_style: it is a hybrid pair interactions containing lj/charmm (for MA to Pb/I) and buck for (Pb and I).
 
```
#INPUT file for lammps

units       real
boundary p p p

atom_style  full

pair_style   hybrid lj/charmm/coul/long 9.9 10. buck/coul/long 9.9 10.
bond_style      harmonic
angle_style     harmonic
special_bonds   amber
dihedral_style  charmm

kspace_style pppm 1.0e-4
kspace_modify diff ad
read_data    lmp.data
include lmp.nonbonding

timestep    0.5

thermo_style custom step temp etotal cella cellb cellc press vol


thermo      1

restart 1000 restart.1a restart.1b
thermo 100

# minimization 
fix 0 all box/relax  aniso 0.0
    minimize 0. 1e-10  1000 1000 
unfix 0

# a quick NPT 
fix 0 all npt temp 1.0 1.0 100.0 aniso 0.0 0.0 1000.0
 dump 0 all atom 10 optimnpt.dump
 dump_modify 0 sort id
 dump_modify 0 scale no
    run 1000
 undump 0
unfix 0

# minimize again
fix 0 all box/relax  aniso 0.0
    minimize 0 1e-10 1000 1000
unfix 0


# a quick NPT to increase temperature
velocity all create 50.0 1234546 dist gaussian
fix 0 all npt temp 50.0 300.0 40.0 aniso 0.0 0.0 1000.0
    dump 0 all atom 1000 npt_heat.dump            # for every 1000 steps (500fs), save to the file npt_heat.dump
    dump_modify 0 sort id
    dump_modify 0 scale no
    run 60000
    undump 0
unfix 0

# npt
fix 1 all npt temp 300.0 300.0 100.0 aniso 0.0 0.0 1000.0
    dump 1 all atom 40 npt_product.dump              # for every 40 steps (20fs), save to the file npt_product.dump
    dump_modify 1 sort id
    dump_modify 1 scale no
    run 200000
    undump 1
unfix 1
```

More detailed pair interaction parameters are included in lmp.nonbonding file as:

```
 pair_coeff           1           3 lj/charmm/coul/long   1.4000000000000000E-002   2.2645400000000002           # Pb -- H charmm 
 pair_coeff           2           3 lj/charmm/coul/long   5.7400000000000000E-002   2.7500000000000000           # I -- H charmm
 pair_coeff           3           3 lj/charmm/coul/long   1.5699999999999999E-002   1.0690999999999999           # H -- H charmm
 pair_coeff           3           4 lj/charmm/coul/long   5.1700000000000003E-002   2.1595000000000000           # H -- N charmm
 pair_coeff           4           4 lj/charmm/coul/long  0.17000000000000001        3.2500000000000000           # N -- N charmm
 pair_coeff           3           5 lj/charmm/coul/long   4.1399999999999999E-002   2.2343999999999999           # H -- C charmm
 pair_coeff           4           5 lj/charmm/coul/long  0.13639999999999999        3.3248000000000002           # N -- C charmm
 pair_coeff           5           5 lj/charmm/coul/long  0.10940000000000000        3.3997000000000002           # C -- C charmm
 pair_coeff           1           6 lj/charmm/coul/long   1.4000000000000000E-002   2.7099899999999999           # Pb -- H charmm
 pair_coeff           2           6 lj/charmm/coul/long   5.7400000000000000E-002   3.1000000000000001           # I -- H charmm
 pair_coeff           3           6 lj/charmm/coul/long   1.5699999999999999E-002   1.5145000000000000           # H -- H charmm
 pair_coeff           4           6 lj/charmm/coul/long   5.1700000000000003E-002   2.6050000000000000           # N -- H charmm
 pair_coeff           5           6 lj/charmm/coul/long   4.1399999999999999E-002   2.6798000000000002           # C -- H charmm
 pair_coeff           6           6 lj/charmm/coul/long   1.5699999999999999E-002   1.9600000000000000           # H -- H charmm
 pair_coeff           1           1 buck/coul/long   70359906.629702002       0.13125800000000001        0.0000000000000000     # Pb -- Pb buck
 pair_coeff           1           2 buck/coul/long   103496.13301000001       0.32173700000000000        0.0000000000000000     # Pb -- I buck
 pair_coeff           2           2 buck/coul/long   22793.338582000000       0.48221700000000001        696.94954199999995     # I -- I buck
 pair_coeff           1           4 buck/coul/long   32690390.937995002       0.15094700000000000        0.0000000000000000     # Pb -- N buck
 pair_coeff           2           4 buck/coul/long   112936.71421300001       0.34242600000000001        0.0000000000000000     # I -- N buck
 pair_coeff           1           5 buck/coul/long   32690390.937995002       0.15094700000000000        0.0000000000000000     # Pb -- C buck
 pair_coeff           2           5 buck/coul/long   112936.71421300001       0.34242600000000001        0.0000000000000000     # I -- C buck
```

Please check the manual for the detailed expressions of [lj/charmm/coul](https://docs.lammps.org/pair_charmm.html#pair-style-lj-charmm-coul-long-command) and [buck/coul](https://docs.lammps.org/pair_buck.html#pair-style-buck-coul-long-command) and what are the parameters needed. If you don't know the meaning of any parameters used here, check the manual for details. 

## HW

:pushpin: use NPT ensemble, run MD and make sure that the system reaches equilibrium (300K and 1 atmosphere) for **both temperature and volume**. Run MD for around 100 ps period. Plot the temperature and volume as a funciton of time. Specify when the system has reached equilibrium. 

:pushpin: There are 256 molecules in this simulation cell. Use the equilibrium trajectory obtained in the last step and compute the A-site molecular dipole (pointing from C (atom index 1205) to N (atom index 1204)) self-correlation function. This self-correlation function is averaged over the MD trajectory. Here, for simplicity, you can assume that the magnitude of each dipole is not changing (bond length does not change and only its direction is changing) during the MD simulation and the magnitude is always 2.1 Debye (1 Debye is around 0.2082 e\*Angstrom, e is electron charge).

:pushpin: Once you obtain the correlation function, use an exponential curve to fit the correlation function and find out the dipole rotation time $`\tau`$.

```math
\bar{C}(t) = A_0 e^{-t/\tau}
```

**Note**: for every time point $`t`$ in the correlation function ($`C(t)`$), enough MD structures are needed to make the average. Then $`t`$ can not be too large, unless your MD length is long enough. (Let's set $`t`$ is from 0 to 15 ps for now.)

:pushpin: Re-do the above calculation but in a lower temperature: 180K (change 300K used in input to 180K for heating and productive NPT run). Re-run MD at this temperature and compute the dipole self-correlation function and $`\tau`$ for this dipole.

:pushpin: Re-do the RDF for LAMMPS-1 water case. Write out the 2D RDF computation formula. Use this program to compute RDF for 2D 250-Ar-atom system: 1) RDF (Ar-Ar) of the random generated initial structure (only one snapshot), 2) RDF (Ar-Ar) of the stimulated annealing structure (take the last snapshot).

\* Acknowledgement: we want to thank Dr. A. Mattoni, Dr. P. Delugas, and Dr. A. Filippetti for their generously provided inputs and structures for this practice.
