# HW2: Steepest descent relaxation method

In this assignment, we will continue the system studied in HW1 and write additional programs to optimize the structure with the steepest descent algorithm. For this method, the change of atomic position $`\Delta R`$ of atom-$`i`$ is proportional to the total force applied to atom-$`i`$ by all the other particles (within the cutoff distance). Therefore, such program should contain the following operations:

### compute the force between atom-i and atom-j given $`r_{ij}`$: $`F_{ij}`$

The analytical expression for force could be computed based on the total energy formula of the LJ energy potential (its first derivative). 

:pushpin: write out the analytical expression for the force between atom $`i`$ and atom $`j`$ (also remember to consider the situtation of the tail function in LJ potential).

### sum over all the atom-j within cutoff to yield the total force on atom-i

Go through all the atom-j and compute $`r_{ij}`$ as well as $`F_{ij}`$. When $`r_{ij}`$ is within the cutoff, add $`F_{ij}`$ to $`F_i`$. 

:warning: note that when computing $`r_{ij}`$, it is necessary to consider the PBC.

### move the atom after obtaining its force

Compute the total forces on all atoms (atoms are not moved when computing the forces), now, we move the atoms at once.

### compute the total energy

If the program of relaxation is correct, the total energy of the system should be **descreasing** all the time. It is necessary to plot the total energy and check.

### exit the cycle loop

When the maximum force (you can compare the magnitude of total force applied on an atom) is below the threshold, we think the structure is already good enough. 

:pushpin: Based on the above descriptions, write a program to perform the structural relaxation following the steepest descent algorithm.

```python
def LJ_pair_energy( rij, Rcut, R0 ):
    ...

def LJ_total_energy( Coords, Rcut, R0, A1, A2 ):
    ...

def LJ_force_atom_i_j( rij, Rcut, R0 ):
    ...

def LJ_force( Coords, Natom, Rcut, R0, A1, A2 ):

    # initialize the total force array for all atoms
    F_all = np.zeros((Natom, 2))

    for i in range(0, Natom):

        # the total force on atom i is stored in F_i
        F_i = np.array([0.0, 0.0])

        for j in range(0, Natom):

            for n in [-1, 0, 1]:
                for m in [-1, 0, 1]:
    
                    # if atom-j is within the cutoff of atom-i (similar to 
                    #   the PBC in total energy calculation)
                    # if rij is below the Rcut, compute the force Fij and 
                    ...
                    F_i = F_i + LJ_force_atom_i_j( rij, Rcut, R0 )

        # simply put F_i to the big matrix of all forces
        F_all[i,:] = F_i[:]

    return F_all

def move_atom( Coords, F_all, alpha, MaxMoveLength ):

    # compute Delta R
    DeltaR =  ...

    # check every atom if DeltaR is larger than the MaxMoveLength
    for i in range(0,NAtom):

        # find the move_length for atom i
        DeltaR_i = ...

        # if such length is larger than MaxMoveLength, set this legnth to MaxMoveLength
        if DeltaR_i >= MaxDeltaR:
            DeltaR[i,:] = ...

        # then modify the coordinates
        Coords[i,:] = ...

    return Coords

def find_max_force( F_all ):
    ...
    return max_force

def SD_relax( Coords, alpha, MaxMoveLength, MaxStep):

    for istep in range(0, MaxStep):

        # compute total energy for current structure
        E_tot = LJ_tot_energy( ... )
    
        # compute all forces for current structure
        F_all = LJ_force( ... )
    
        # move atoms based on the force to obtain new coordinates
        Coords = move_atom( ... )
    
        # print out necessary info for plotting: step , total energy, and max force, etc
        print ( ... )

        # if it is good enough to exit relaxation?
        ...

        # save the coordinates of every step to the file
        np.savetxt( str(istep)+'.dat', Coords )

    return


# setup (similar to total energy program)
...

# start relaxation
SD_relax( ... )

```

:pushpin: based on the above outlines of programs, write your program of computing force and relax the system.

:pushpin: starting with the coordinates in the following link (same to HW1) and relax the structure until the max force is below 0.5 eV/Angstrom. Plot the total energy, max force as a function of the step (in seperate figures). Plot the initial and final structures for comparison.

![coordinates-250particles](coordinates.dat)

:pushpin: starting with the randomly generated coordinates from your last homework and relax the structure until the max force is below 0.5 eV/Angstrom. Plot the total energy, max force as a function of the step (in seperate figures). Also plot the initial and final structure for easy comparison.

**Note**: if the first few total energies (or forces) are too large to be plotted in a graph nicely, you can plot starting from later steps but list the first few values in a table following the plot.

**Requirement**: upload both the programs/scripts and the solutions to TA before deadline. The solutions to the above questions should be summarized in a **self-explained report** style document (save as .pdf file)}.


