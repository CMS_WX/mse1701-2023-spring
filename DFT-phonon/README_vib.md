Use DFT to obtain vibrational frequencies
=============================

With silicon and aluminum as examples, we will learn how to use DFT with QE to calculation $`\Gamma`$ or X point vibrational frequencies and vibrational modes. 

## Phonon calculation for Silicon $`\Gamma`$ point

QE needs SCF calculation generated charge density for phonon calculation. Use the previous input file to perform a SCF first.

A great thing of QE is that it uses density functional perturbation theory (DFPT) to obtain phonon frequencies and vibrational modes. It means a phonon at a generic $`q`$ point (phonon wavevector) could be computed using a unit cell. Conventional method such as real space dynamic matrix has to use supercells.

Let's see the input file of $`\Gamma`$ point phonon calculation in_ph (**Note**, remove '#' and comments after '#' before submitting the job):

```fortran
Silicon primitive cell Gamma phonon
&INPUTPH
  prefix = 'silicon',                  # this should be consistent to 'prefix' used in SCF calculation
  outdir = './',
  verbosity = 'high',
  recover = .false.,
  amass(1) = 28.085,                   # mass of atom type 1, this is important!!! specify the correct mass
  tr2_ph = 1e-13,
  fildyn = 'silicon-matdyn',
  trans = .true.,
/
0  0  0                                # coordinates for q point (in terms of 2pi/a0), here only Gamma point is computed (which is the BZ center)
```

Phonon calculation is much more expensive than SCF and it takes much longer time (but not that crazily long for the silicon system). Remember to use `ph.x < in_ph > out_ph` instead of `pw.x < in_scf > out_scf`.

Remember the acoustic sum rule (ASR)?!! After the calculation, particularly at $`\Gamma`$ point, the ASR is needed to shift the acoustic mode frequencies to be zero at $`\Gamma`$. Setup ASR input as (in_dynmat):

```fortran
&input
  fildyn = 'silicon-matdyn',
  amass(1) = 28.085,
  asr = 'crystal',
/
```

Run `dynmat.x < in_dynmat > out_dynmat`. Since this is a fast post-processing calculation, use single core is enough. Now see the output out_dynmat and find out the phonon frequencies at $`\Gamma`$ point. Open dynmat.out (or dynmat.axsf) to see the vibrational mode for each mode and each atom. Since $`\Gamma`$ point is a high symmetry point, all the modes are real (the imaginary part is zero), which means they can be plotted! 

![An example of Silicon dynmat.out.](phonon.png)

:pushpin: How many phonon modes in total at $`\Gamma`$ point for silicon primitive cell? How many of them are optical modes and acoustic modes, respectively? Use the above method to compute Si phonon and list the frequencies at $`\Gamma`$ point. Use VESTA to plot the phonon modes (you should have one figure for each mode). 

In dynmat.out, all phonon modes are listed (with their frequencies). For each phonon mode, the direction for each atom is also shown as a complex three-elements vector indicating x, y, and z directions. Here, we only plot the real part. 

**Tips**: How to plot arrow in VESTA automatically, simply append the arrow coordinates to each atomic position in \*.xsf file and read into VESTA. e.g.

    Si   position(x)  position(y)  position(z)  arrow_x  arrow_y  arrow_z
    Si    ...  ...


## Phonon calculation for Aluminum X and $`\Gamma`$ point

Here, to save time, let's use 50 Ryd E_cut and 16x16x16 k point grid (which are already converged).

```fortran
 &control
    calculation='scf'
    restart_mode='from_scratch',
    prefix='aluminum',
    pseudo_dir = './',
    outdir='./'
 /
 &system
    ibrav=  2, celldm(1) =7.6, nat= 1, ntyp= 1,
    ecutwfc =50.0,
    occupations='smearing', smearing='marzari-vanderbilt', degauss=0.05,
 /
 &electrons
    conv_thr =  1.0d-8
    mixing_beta = 0.7
 /
ATOMIC_SPECIES
 Al  26.98  Al.SG15.PBE.UPF
ATOMIC_POSITIONS
 Al 0.00 0.00 0.00
K_POINTS {automatic}
 16 16 16  0 0 0
```

The above input is a little bit different from Silicon case. Here, some quick lattice structure setup is used (e.g. use ibrav=2). If you are interested, check out pw.x [manual](https://www.quantum-espresso.org/Doc/INPUT_PW.html#idm119) and find out info about *ibrav*.

:pushpin: After SCF convergence, compute and plot the band structure of Al for the following k point path $`\Gamma`$ (0, 0, 0) --> X (0.5, 0, 0) --> M (0.5, 0.5, 0) --> $`\Gamma`$ (0, 0, 0) --> R (0.5, 0.5, 0.5). When using titledbandstrv5.1.py for metals, remember to provide Fermi energy by flag `-f fermi_energy`. The value of Fermi energy can be obtained in the output file of **SCF** instead of bands output (check it out near final total energy line). 

![Example of Fermi energy in metal SCF calculations](fermi.png)

```python
python titledbandstrv5.1.py out_bands num._of_valence_bands  4 4  -f fermi_energy_value_input
```

Here, `num._of_valence_bands` can use any number. Once `-f` is specified, the script will not use number of valence bands (but still read this number anyway).

The input of Al phonon is similar to Si except the mass and last line. The following input is for X point. For $`\Gamma`$ point, simply use `0 0 0` (same to silicon case).

```fortran
phonons of Al at X
 &inputph
  prefix='aluminum',
  outdir='./',
  verbosity = 'high',
  recover = .false.,
  amass(1)=26.982
  tr2_ph=1.0d-14,
  fildyn='Al-X.dyn',
  trans = .true.,
 /
1.0 0.0 0.0
```

The last line is the coordinates of X point. **Note** this coordinate is written in terms of 2pi/a instead of reciprocal lattices. 

:pushpin:  How many phonon modes for this system at $`\Gamma`$ point and X point? Compute and list the vibrational frequencies at X and $`\Gamma`$ of Al (X point does not have the issue of ASR; only apply ASR to $`\Gamma`$ point). 

