# Verlet MD simulation

Based on the exercise of HW1 and 2, it should be straightforward to write a program for MD simulation. In this assignment, I will only outline the most important parts.


```python
def MD(StartStep, EndStep, Coords, Vel, r0, rcut, NAtom, Temperature_target, dt, NVT=False )

    # compute the force given coordinates
    F_all = 

    for istep in range(StartStep, EndStep):

        # Update coordinates
        Coords = ...

        # Force of new coordinates
        F_all = ...

        # Update velocity
        Vel = ...

        # new potential energy
        E_pot = ...

        # kinetic energy
        E_kin = ...

        # total energy
        E_tot = E_pot + E_kin

        # Temperature
        Temmperature = ...

        # update temperature? (perform NVT?)
        if NVT:
           ...
           ...

        if istep%50==0: np.savetxt( 'MD-'+str(istep)+'.dat', Coords )

        # print info
        print (...)

    return Coords, Vel


# start main program

# compute and setup the initial velocity, force, etc
Velocity = ...
F_all = ...
E_pot = ...
E_kin = ...
Temperature = ...
E_tot = E_kin + E_pot

# print info for initial condition
print (...)

# start MD
# set NVT=False or True 
Coords, Vel = MD( ... NVT=True) 

```

:pushpin: Based on the above outlines of programs, write your program of NVE and NVT MD simulations.

:pushpin: Using the **relaxed** structure from HW2 (relax based on the **given** coordinates) as the initial structure, set up the initial temperature to 600K, take $`dt=1`$ fs, and run the **NVE** MD for a total of 1ps (1000 steps). Plot the following quantities.
                                                                         
:pushpin: Using the **relaxed** structure from HW2 (relax based on the **given** coordinates) as the initial structure, set up the targe temperature to 600K, take $`dt=1`$ fs, and run the **NVT** MD for a total of 1ps (1000 steps). Plot the following quantities.

* Plot the total energy as a function of time.

* Plot the potential energy as a function of time.

* Plot the kinetic energy as a function of time.

* Plot the temperature as a function of time. 

* Plot the structure of the final step.

:pushpin: Now, let's perform a so-called stimulated annealing to heat the structure then cool it down. Perform the following MD simulations:

* Taking the HW2 relaxed structure (the relaxed structure is based on the **random** coordinates one) as the initial structure, with NVT MD, set up the target temperature to **400K** and run MD for **2ps** ($`dt=1`$ fs).

* use the above final step structure as the initial structure, with NVT MD, set up the target temperature to **300K** and run MD for 0.5ps ($`dt=1`$ fs).

* use the above final step structure (300K MD) as the initial structure, with NVT MD, set up the target temperature to **250K** and run MD for 0.5ps ($`dt=1`$ fs).

* continue the above loop but with different temperatures as **200K**, **150K**, **100K**, **50K**, **30K**, **10K**, then stop (at each temperature run MD for 0.5 ps).

:pushpin: plot the temperature as a function of time for the above all steps (put above all MD simulations at different temperatures in one figure, so the total time length of this figure is 5ps).

:pushpin: Take the *final* MD structure as the initial, use HW2 relaxation code to **relax** such structure to get a new structure. Now, we have **two** relaxed structures: 1 from the HW2 direct relaxation, and 2 from a relaxation but following the stimulated annealing process. Compare the total energies of the two structures, and plot these two structures for comparison.

 
