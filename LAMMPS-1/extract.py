

import numpy as np
import sys
import re


def convert_listofstring2float( ListOfString ):
    s2f = lambda u: [float(i) for i in u.split()]
    C = np.array( [s2f(i) for i in ListOfString] )
    return C


def extract_dump_from_LAMMPS( RawInput, NAtom, NAtomType=None, Sort=False ):

    NLinePerImage = NAtom + 9

    NImage = int(len(RawInput)/NLinePerImage)

    Coords = []; Names = []; Lats = []

    for i in range(0, NImage):

        Lat = convert_listofstring2float( RawInput[i*NLinePerImage+5:i*NLinePerImage+8] )
        Lats.append( Lat )

        ParseArr = convert_listofstring2float( RawInput[i*NLinePerImage+9:i*NLinePerImage+NLinePerImage] )
        Name = ParseArr[:,:2].astype(int)
        Coord = ParseArr[:,2:].astype(float)

        if Sort:
            NameOrder = np.argsort( Name[:,0] )
            CoordOrder = Coord[NameOrder]
            Coords.append( CoordOrder )
            Names.append( Name[NameOrder] )
        else:
            Coords.append( Coord )
            Names.append( Name )

    return Lats, Names, Coords






# Start main

with open(sys.argv[1],'r') as f:
    RawInput = f.readlines()


NAtom = 600


# Here, Coords stores all the step and all the atom coordinates.
#       Names stores all the atomic index/species for all steps.
#       Lats stores all the lattices for all steps.

# For each step, all atom coordinates are stored as an element of Coords (Coords is a list). 
#   So each element of Coords is a 2D np.array, which has dimension as NAtom * 3 (Natom rows and 3 colums).
# Same style for Lats and Names, i.e. each step Lattice (or Name) are stored as an element of Lats (or Names).

#   ==> Each element of Lattice is a 3 x 2 array       (type float)
#   ==> Each element of Coords is a NAtom x 3 array (type float)
#   ==> Each element of Names is a NAtom x 2 array  (type int)
#           each element of Names is a NAtom x 2 array, the first column is atom index (from 1 to 600), and 
#           second column is atomic species: 1 means oxygen and 2 means hydrogen
# You can print a few images of Names and Coords and compare with the LAMMPS output to see what's going on

Lats, Names, Coords = extract_dump_from_LAMMPS( RawInput, NAtom )


# for example, the following you print will be the first step (use [0]) atomic index , symbol and 3D coordinates
for i in range(0, 600):
    print ('%d  %d  %12.6f %12.6f %12.6f'%(Names[0][i,0],Names[0][i,1], Coords[0][i,0], Coords[0][i,1], Coords[0][i,2]))

# for example, the following you print will be the 120 step (use [119]) atomic index , symbol and 3D coordinates
for i in range(0, 600):
    print (Names[119][i,:], Coords[119][i,:])

