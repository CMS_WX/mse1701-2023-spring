# HW 1: Argon atoms in a 2D box (total energy)

Setup a two dimensional square box as the unit cell with $x$-length 6.3 nm and $y$-length 6.3 nm. Put 250 Ar atoms into this box with *random coordinates*. 

:pushpin: Plot the box and all the argon atoms in a figure (atoms can be plotted as dots/plus sign/asterisk sign etc, box boundary can be plotted with solid lines). 


Assuming Ar-Ar atomic interaction only has LJ potential. The LJ parameter is $`\epsilon=0.185`$ kcal/mol (or 0.00802236 eV) and $`\sigma=3.97`$ Angstrom. Let's set $`r_{\rm cut}=11`$ Angstrom; and $`r_0=10`$ Angstrom (a larger $`r_{\rm cut}`$ than 11 Angstrom will introduce very small energy change which can be neglected).

You can use the tail function like:
```math
U^{\rm tail}(r_{ij}) = A(r_{ij}-r_{\rm cut})^4 + B(r_{ij}-r_{\rm cut})^2
```

:pushpin: Use the continuous conditions to obtain the values for $A$ and $B$.

:pushpin: Write a program to compute the total energy for given coordinates of such 2D system. Remember it is necessary to consider the **periodic bounday condition (PBC)**. 

Following is an outline for a typical PBC program. You can write your program on your own or you can modify the following program.

```python
def LJ_total_energy(Coords, Rcut, R0, A1, A2):
    ```Coords: 2D array with each row as the [x, y] 
                coordinates for one atom
       Rcut: Rcut for LJ potential
       A1: lattice vector a1 (a 3-element vector)
       A2: lattice vector a2 (a 3-element vector)
    ```
    TotalEnergy = 0.0

    NAtom = np.shape( Coords )[0] 

    for i in range(0, NAtom):

        # now we find all the atom-j within the cutoff to atom-i
        for j in range(0, NAtom):
  
            for nx in [-1, 0, 1]:
              for ny in [-1, 0, 1]:
    
                # shift the coordinates-j to its neighbor image (think how to shift the coordinates)
                CoordJShift = Coords[j,:] + ...
                
                # compute distance between atom i and shifted atom j
                rij = ...
                
                # if rij < Rcut, compute LJ energy
                # ** think why we also require math.isclose(rij,0.0) to be false ? **
                if rij < Rcut and (not math.isclose(rij,0.0)):
      
                TotalEnergy = TotalEnergy + LJ_pair_energy(rij, Rcut, R0 ) * 0.5
    
              ... ...

    return TotalEnergy
```

:pushpin: why we only count j with `math.isclose(rij,0.0)` false in the above program?

Your main program can be written as:

```python
import numpy
import math

def LJ_pair_energy(rij, Rcut, R0):
    A = 
    B = 
    ...
    ...

def LJ_total_energy(Coords, Rcut, R0, A1, A2):
    ...
    ...

# set up the lattice
A1 = ...
A2 = ...

# set up the cutoff
Rcut = ...
R0   = ...

# set up the filename for the coordinates we want to compute
FileName = ...

# reading the coordinates from file Filename
Coordinates = np.loadtxt(FileName, dtype=float)

# compute the total energy
TotalEnergy = LJ_total_energy( Coordinates, Rcut, R0, A1, A2 )

# print out the computed total energy (note the unit)
print ('The total energy of the 2D system is ', TotalEnergy)
```

:pushpin: Use the program to compute the total energy for the randomly distributed 2D system.

:pushpin: Download the coordinates (Angstrom unit) from the following link and use the program to compute the total energy.

![coordinates-250particles](coordinates.dat)

**Requirement**: upload both the programs/scripts and the solutions to TA before deadline. The solutions to the above two questions should be summarized in a **self-explained report** style document (save as .pdf file)}.


