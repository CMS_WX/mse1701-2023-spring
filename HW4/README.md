# MD and diffusion

Based on the MD trajectory at different temperatures, let's compute the particle diffusion coefficient.


```python

# if your particles is manually put back to the box, attention is needed for the distance 
def comp_dist( R_current, R_0, R_prev, LatLength ):
    
    DistAll = np.zeros( 9 )
    i = 0
    for m in [-1, 0, 1]:
        for n in [-1, 0, 1]:
            # shift the particle
            DistAll[i] = ...
            i = i + 1
    # use the shortest
    Dist = np.min( DistAll )
    return Dist

def read_traj_and_comp_R2( StepStart, StepEnd, Prefix, Lattice, NAtom ):

    NImag = StepEnd - StepStart + 1

    DeltaR2 = np.zeros( (NAtom, NImag) )

    for i,istep in enumerate( range(StepStart, StepEnd+1) ):

        # read in the coordinates for i-th step
        FileName = Prefix + str(istep) + '.dat'
        Coords = np.loadtxt( FileName, dtype=float )

        # setup r0
        # note that you HAVE TO use np.copy here to have a deep copy of an array without reference
        if istep == StepStart: 
            Coords0 = np.copy( Coords )
            # Coords_old is needed if you pull the particle back to box
            Coords_old = np.copy( Coords )

        # compute R2 for j-th particle
        for j in range(0, NAtom):

            Dist = ...

            DeltaR2[j,i] = Dist**2

        print ('Finishing step ' + str(istep))

        # Coords_old is needed if you pull the particle back to box
        Coords_old = np.copy( Coords )

    return DeltaR2


# compute R^2 for all particles
DeltaR2_all = read_traj_and_comp_R2( StepStart, StepEnd, Prefix, Lattice, NAtom )

# sum over and find out the average
DeltaR2 = np.sum( DeltaR2_all, axis=0 )/NAtom

# print DeltaR^2 as a function of time
...

```

:pushpin: Use the NVT MD code (used in HW3) to run MD for the Ar system (download the [coordinates](coordinates.dat), same to previous HW) at different temperatures: 400K, 350K, 300K, 250K, 200K, and 100K. For each temperature, run the MD for roughly 2 ps with $`dt=1`$ fs (around 2000 steps). Save the coordinate for every step (remember the numpy.savetxt( FileName, Coords ) could be used here). For each temperature case, plot the temperature as a function of time and specify when the system reaches equilibrium.
    
For each temperature, write a script to compute the diffusion constant using the relation: $`\Delta \mathbf{R}^2(t) = 4Dt`$. Here, $`\Delta \mathbf{R}^2(t)`$ is averaged over all 250 particles as $`\Delta \mathbf{R}^2(t) = 1/400 \sum_i \Delta \mathbf{R}_i^2(t)`$. 

:pushpin: Randomly choosing 8 particles and plot their $`\Delta \mathbf{R}^2(t)`$ at temperature=300K as a function of time (plot these 8 lines in a single figure).

:pushpin: For each temperature, plot the the averaged $`\Delta \mathbf{R}^2(t)`$ (it should be roughly a straight line) and use a straight line to fit to obtain the slop to get the diffusion coefficient.
    
:pushpin: Using the Einstein relation, plot the mobility as a function of temperature. 

:warning: **Some advices if you manually pull the particle back to box after each MD step**

<img src="hw4.png" width="400" height="290">

There is something you need to be careful when computing $`\Delta \mathbf{R}_i^2(t)`$ for particle $`i`$. (By the way, most of the codes (including LAMMPS) will also pull the particles back to the box if it is leaving it.) Here is why you need to pay attention: If your program manually pulls the out-of-box particle back to the box using periodic boundary condition after one or serveral MD steps, remember when computing $`\Delta \mathbf{R}_i^2(t)`$, one may not want to compute the distance directly after you pull the particle back. Otherwise $`\Delta \mathbf{R}_i^2(t)`$ will have a big jump and $`\Delta \mathbf{R}_i^2(t)`$ should be a continuous function. *Hint*: you can track the one-step distance the particle travels between $`\mathbf{R}_i^2(t=t)`$ and $`\mathbf{R}_i^2(t-dt)`$. Enumerate the 9 boxes $`m, n=(-1,0), (0,0), (0,1)`$ etc and find out the shortest one-step distance, which should be the *real* distance it travels during such $`dt`$ step.


