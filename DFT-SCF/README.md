Perform first DFT calculation
=============================

Using silicon as an example, we will learn DFT - self-consistent calculation (SCF) with the open source software [**Quantum Espresso(QE)**](https://www.quantum-espresso.org/) (QE) to perform DFT calculations.

## SCF calculation

In this step, using silicon as an example, we will learn how to build up a simple model for calculation.


### Before we start, load modules first

When using ShanghaiTech HPC, after we log in, load the python module you used before. QE doesn't need python, but some of the scripts will use it to build structure or visuallize results.


### Input files for QE calculation 

Different from LAMMPS, QE will include all input parameters and structure in *one* file. Here is its structure:

The details of the parameters can be found in the QE manual [here](https://www.quantum-espresso.org/Doc/INPUT_PW.html)

    &control                            # "&flag ... /" is called namelist in Fortran programing (QE is written with Fortran and C languages)
       calculation = 'scf'              # specify this is a SCF calculation: SCF stands for self-consistent field which yields total energy, force, stress, ... for given structure
       restart_mode='from_scratch',     # if start from scratch use "from_scratch"; if start from a break point use "restart".
       prefix='silicon',                # give a prefix for other output files 
       tstress = .true.                 # if print stress (yes here)
       tprnfor = .true.                 # if print force (yes here)
       pseudo_dir = './'                # where is pseudopotential (psp) file? (here "./" means IN THIS FOLDER)
       outdir='./'                      # where should the program output its outputfiles? (here "./" means IN THIS FOLDER)
       verbosity = 'high',
    /
    &system
       ibrav=  0,                    # we use type 0 in order to specify the lattice vectors
       nat=  2,                      # we have two atoms in one cell for computation
       ntyp= 1,                      # we only have one type of atoms (i.e. Si)
       ecutwfc =50.0,                # cutoff energy for planewaves (Ryd unit)
       nspin = 1,                    # consider spin: 1 (no spin), 2 (yes with spin)
       occupations = 'fixed',        # how is the electron occupation determined? (fixed: for gapped system, otherwise use smearing)
       nbnd = 10,                    # total number of bands (for each k point) is 10, the lowest 4 of them are occupied; others are unoccupied
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            # when energy difference between two successive step is smaller than this number, the program thinks SCF is converged
    /
   
    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF    # specify atom species and the psp it uses, the middle number is the mass for this type of atoms
    
    K_POINTS {automatic}             # how to sample k point, here we sample the first BZ with 8x8x8 k point
     8  8  8  0  0  0
    
    CELL_PARAMETERS (Angstrom)
      0.00000000     2.71535000     2.71535000    # lattice vectors. Here, the program reads lattice vector will be this 3x3 matrix
      2.71535000     0.00000000     2.71535000    #  here, these lattice vectors are in unit of ANGSTROM 
      2.71535000     2.71535000     0.00000000
    
    ATOMIC_POSITIONS (crystal)
      Si     0.00000000     0.00000000     0.00000000   # atomic positions (here use fractional coordinates)
      Si     0.25000000     0.25000000     0.25000000
	

The above parameters are in file in_scf. (You can use other filename you like). **Please remove '#' and comments after #, in Fortran, comment is not marked with #**


### pseudopotential files

For each atomic type, there will be a pseudopotential file. Here, we use Si.SG15.PBE.UPF to represent Si pseudopotential. (Also setup this file in in_scf.)


### Prepare for job launching

Put input file ("in_scf"), pseudopotential file (Si.SG15.PBE.UPF), and job script ("job.pbs" one can use any name for it) into the **same** directory to perform this calculation.
The output file is set in the job script as **out_scf** or any other name you like. If this file is not setup, it will print on the screen. 

    #!/bin/bash
    #PBS  -N   test                   # job name
    #PBS  -l   nodes=1:ppn=1          # resource request: here request 1 node, each node request 1 cores
    #PBS  -l   walltime=0:15:0        # set up a walltime (one should have an estimate of a job, usually a short walltime means fast queue time)
    #PBS  -S   /bin/bash
    #PBS  -j   oe 
    #PBS  -q   pub_jx                 # for CPU job, use "pub_jx" queue
    
    cd $PBS_O_WORKDIR
    
    NPROC=`wc -l < $PBS_NODEFILE`
    
    echo This job has allocated $NPROC proc > log
    
    module load apps/quantum-espresso/intelmpi/6.7    # those are the modules used for QE
    module load compiler/intel/2021.3.0
    module load mpi/intelmpi/2021.3.0
    
    mpirun  --bind-to core -np $NPROC -hostfile $PBS_NODEFILE pw.x  < in_scf >& out_scf             # input file is in_scf, output file is out_scf



### Launch the job

Following the manual of the machine (each machine may have different job script or launching style).

	$ qsub job.pbs                         # submit job

	$ qstat                                # check job status, check manual to see different argument for squeue

	$ qdel jobid                           # kill the job by specifying the job id


### Output files

Most information of output will be in file out_scf. Other output files will include more detailed info (check the QE manual to see what they are).

**Some basic info about the system**

     bravais-lattice index     =            0
     lattice parameter (alat)  =       7.2567  a.u.
     unit-cell volume          =     270.2116 (a.u.)^3
     number of atoms/cell      =            2
     number of atomic types    =            1
     number of electrons       =         8.00
     number of Kohn-Sham states=           10
     kinetic-energy cutoff     =      50.0000  Ry
     charge density cutoff     =     200.0000  Ry
     ...

**k-point list**

     number of k points=    29
                       cart. coord. in units 2pi/alat
        k(    1) = (   0.0000000   0.0000000   0.0000000), wk =   0.0039062
        k(    2) = (   0.0883883   0.0883883  -0.0883883), wk =   0.0312500
        k(    3) = (   0.1767767   0.1767767  -0.1767767), wk =   0.0312500
        k(    4) = (   0.2651650   0.2651650  -0.2651650), wk =   0.0312500
        ...

**SCF loop**

     Self-consistent Calculation

     iteration #  1     ecut=    50.00 Ry     beta= 0.70
     Davidson diagonalization with overlap
     ethr =  1.00E-02,  avg # of iterations =  6.8
     ...
     total energy              =     -15.76427529 Ry
     estimated scf accuracy    <       0.05682308 Ry

     iteration #  2     ecut=    50.00 Ry     beta= 0.70
     Davidson diagonalization with overlap
     ethr =  7.10E-04,  avg # of iterations =  1.2
     ...
     total energy              =     -15.76715309 Ry
     estimated scf accuracy    <       0.00201487 Ry

     iteration #  3     ecut=    50.00 Ry     beta= 0.70
     Davidson diagonalization with overlap
     ethr =  2.52E-05,  avg # of iterations =  6.0
     ...

     ...
     End of self-consistent calculation

**Converged eigen energies**

          k = 0.0000 0.0000 0.0000 (  1639 PWs)   bands (ev):

    -5.7339   6.2131   6.2131   6.2131   8.7715   8.7715   8.7715   9.5595
    13.8295  13.8296

     occupation numbers
     1.0000   1.0000   1.0000   1.0000   0.0000   0.0000   0.0000   0.0000
     0.0000   0.0000

          k = 0.0884 0.0884-0.0884 (  1606 PWs)   bands (ev):

    -5.5299   4.6193   5.9279   5.9279   8.7019   9.1921   9.1922  10.9063
    13.3633  13.3635

     occupation numbers
     1.0000   1.0000   1.0000   1.0000   0.0000   0.0000   0.0000   0.0000
     0.0000   0.0000
          ...

**Total energy and Energy decomposition**

```
!    total energy              =     -15.76729291 Ry
     estimated scf accuracy    <          9.8E-09 Ry

     The total energy is the sum of the following terms:
     one-electron contribution =       4.74259266 Ry
     hartree contribution      =       1.10299372 Ry
     xc contribution           =      -4.81610084 Ry
     ewald contribution        =     -16.79677844 Ry
     ...
```

### Converge the cutoff energy and k point sampling

**k point sampling:**

:pushpin: By setting 50Ryd cutoff energy (50Ryd is enough for most materials), let's converge the SCF k point such as 2x2x2, 4x4x4, 6x6x6, 8x8x8, and 10x10x10. Compare the total energy for each k point sampling and plot.

**cutoff energy:**

:pushpin: With converged k point grid (obtained from above calculation), set cutoff energy from 20, 30, 40, 50, 60, and 70 Ryd. Compare the total energy for each cutoff energies and plot.

After the calculation, let's find the appropriate cutoff energy and k point sampling. For small system (e.g. Si we use here), we could apply a relatively strict convergence threshold (since the calculation is relatively *cheap*). Here, let's say 0.002 eV/atom for Si case. It means, for example, if E\_cut(20Ryd) has total energy -151.000 eV, E\_cut(30Ryd) = -151.030 eV, E\_cut(40Ryd) = -151.031 eV, we say 30 Rdy is enough. This is because abs(E(30Ryd) - E(40Ryd))/per atom = 0.001 eV, which is already smaller than 0.002 eV/atom (two atoms per unit cell). 

For more complex system, the computational cost increases significantly (~n^3, n is number of electrons). Usually, we can use ~0.02 eV/atom as a threshold. But sometimes it depends on the computational cost, particularly for very expensive calculation. 

:pushpin: compare the above k sampling and cutoff energy convergence test and choose the appropriate E\_cut and k point grid.

:pushpin: Use $`\Gamma`$ point as an example (find the block for output k-point eigen energies), please count how many states are occupied and how many are empty. A simple way for non-spin gapped material is that the number of valence band is num. of electrons divided by 2. Also find out what is the eigen energy for $`\Gamma`$ point highest occupied state and the eigen energy for $`\Gamma`$ point lowest unoccupied state.

### Practice: SCF calculation for GaAs

Use the cooridnates of GaAs from the file GaAs.coord and compose an SCF input for GaAs.

Similar to the silicon case above, let's converge **k point sampling** and **cutoff energy**. Use 0.02 eV/atom as the threshold.

:pushpin: plot k sampling and cutoff energy convergence test and choose the appropriate E\_cut and k point grid.


## Plot band structure

Band structure is the eigen states solved for some **specific k point path** (such k-point path is not a regular k point sampling in a SCF calculation). Band structure is usually the most direct observation of the electronic structure of a material. Since bands calculation is non-SCF, a SCF (or a full relaxation) must be done. And band calculation is performed based on the charge density obtained in SCF simulation. First, make sure the SCF or relaxation calculations are fully converged (in terms of Ecut and SCF k point sampling). 

OK, after the SCF calculation, we will plot the band structure of Silicon.

### Obtaining the k point path 

In the same directory where we run relax, have a new input file "in\_bands". First, let's decide the k point path (i.e. band structure x-axis). We will sample the k point along the following lines (lines starts and ends are called **high symmetry k point, e.g. $`\Gamma`$, X, M or R**):

    from \Gamma (0.0, 0.0, 0.0) to X (0.5, 0.0, 0.0) sample 4 k point *evenly* between \Gamma and X;
    from X (0.5, 0.0, 0.0) to M (0.5, 0.5, 0.0) sample 4 k point *evenly* between X and M; 
    from M (0.5, 0.5, 0.0) to \Gamma (0.0, 0.0, 0.0) sample 4 k point *evenly* between M and \Gamma; 
    from \Gamma (0.0, 0.0, 0.0) to R (0.5, 0.5, 0.5) sample 4 k point *evenly* between \Gamma and R; 

The 4 k-points do not include the **two ends**. So in total 21 k point. (:pushpin: You need to figure out all the k point coordinates along the above 4 lines.)

### Prepare the input file

The following is a template for bands calculation (file: in\_bands): **please remove '#' and comments after #, in Fortran, comment is not provided with #**

    &control                         
       calculation = 'bands'                              # this is the change, change to bands to perform non-SCF calculation
       restart_mode='from_scratch',     
       prefix='silicon',                
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       outdir='./'                      
       verbosity = 'high',
    /
    &system
       ibrav=  0,                    
       nat=  2,                      
       ntyp= 1,                      
       ecutwfc =50.0,                                     # use the same cutoff energy to scf or relax
       nspin = 1,                    
       occupations = 'fixed',        
       nbnd = 10,                                         # Here, one has to specify more bands, otherwise it only prints out valence bands (total band here is 10)
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8            
    /
   
    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF    
    
    K_POINTS {crystal}                                    # the generate 21 k point coordinates should be fractional coordinates
     21 
    0.0000  0.0000  0.0000    1.0                         # first three numbers are k point coordinate, the last number is the kpoint weight. Since we are doing non-scf, the weight is not import and set to 1 for all k points
    0.1000  0.0000  0.0000    1.0                         # there should be 21 lines for k point coordinates
    0.2000  0.0000  0.0000    1.0
      ...
    0.4000  0.0000  0.0000    1.0
    0.5000  0.0000  0.0000    1.0
    0.5000  0.1000  0.0000    1.0
      ...
    0.5000  0.5000  0.0000    1.0
      ...
      ...

    CELL_PARAMETERS (Angstrom)
      0.00000000     2.71535000     2.71535000   
      2.71535000     0.00000000     2.71535000   
      2.71535000     2.71535000     0.00000000
    
    ATOMIC_POSITIONS (crystal)
      Si     0.00000000     0.00000000     0.00000000
      Si     0.25000000     0.25000000     0.25000000


### Launch the job and plot band structures

Please set the output file to be "out\_bands", it will print all the eigen values for each k point out of the 21 k points. 

In the same directory, run the command:

```bash
python titledbandstrv5.1.py out_bands num._of_valence_bands  4 4 
```

num.\_of\_valence\_bands: the number of valence bands counted from file out\_bands  
out\_bands: output file of bands calculation
4: starting energy of y-axis of the band structure is Fermi energy minus 4 eV  
4: ending energy of the y-axis of the band structure is Fermi energy plus 4 eV  

This script will generate a .png file. Download this file and check it out.

We still find that with 21 k points, parts of the bands are *not* that smooth. :pushpin: In this case, more k points can be sampled between two high symmetry k point: instead of 4, let's try 40 to see if it will work. 

:pushpin: compute and plot the band structure of silicon (including the coarse case (4 kpoint along one path) and the fine case (40 sampled k points along one path).


