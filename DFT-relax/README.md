Structural relaxation
=====================

## "RELAX" calculation

For most cases, the structure one starts with is not usually the most *optimized* structure (i.e. atoms are not in their equilibrium position yet). It is necessary to let the program to relax the structure and minimize the forces on each atom.

The way QE (including other softwares) performs relaxation is that after computing total energy and force (given initial structure), it moves the atoms based on their forces, then re-calculate the new total energy and forces. Repeat this process until the **maximum forces** on each atom is below the threshold set in the input files.

### Silicon exmample

Find out the slicon SCF input file (e.g. in_scf), now let's manually shift the first silicon atoms a *little bit* away from its equilibrium position. For example, the first silicon atom coordinates should be (0.000   0.000   0.000), but let's change it to (0.050   0.050   0.050).

Now the input file for relaxation (file: in_relax) is (**remember to use the converged cutoff energy and k point sampling. Also please remove '#' and comments after #, in Fortran, comment is not provided with #**):

    &control
       calculation = 'relax'                             # this is the change, change scf to relax to perform relaxation
       restart_mode='from_scratch',
       prefix='silicon',
       tstress = .true.
       tprnfor = .true.
       pseudo_dir = './'
       forc_conv_thr = 0.0002d0,                         # when the maximum forces on all atoms are below 0.0002 Ryd/Bohr, the relaxation will terminate
       nstep = 2000,                                     # the maximum of relaxation steps is 2000
       outdir='./'
       verbosity = 'high',
    /
    &system
       ibrav=  0,
       nat=  2,
       ntyp= 1,
       ecutwfc =50.0,                                    # use the converged total energy
       nspin = 1,
       occupations = 'fixed',
       nbnd = 10,
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-8
    /

    &ions                                                # the &ions / namelist is required to relax atomic positions, although all the 
                                                         #  parameters related to relaxation could use default values
    /

    ATOMIC_SPECIES
      Si   28.08  Si.SG15.PBE.UPF

    K_POINTS {automatic}
     8  8  8  0  0  0                                     # use the converged k point grid

    CELL_PARAMETERS (Angstrom)
      0.00000000     2.71535000     2.71535000
      2.71535000     0.00000000     2.71535000
      2.71535000     2.71535000     0.00000000

    ATOMIC_POSITIONS (crystal)
      Si     0.05000000     0.05000000     0.05000000
      Si     0.25000000     0.25000000     0.25000000

Here, the difference of the input parameters from the previous SCF inputs is that the flag `calculation` is set to `'relax'`. Relaxation calculation will be longer than SCF, since it is repeating SCF calculations. After the relaxation is finished, check the final structure in out_relax (or other filename set up in the job script), what are the coordinates of silicon atoms (including the manually shifted one). If the two silicon-atom difference is still not (0.25, 0.25, 0.25) (or (0.75, 0.75, 0.75) if shifting a whole unit cell), try using a smaller force convergence threshold (such as 0.0001).

### Check the relaxation structure (**Important**)

In the directory, run the command "python read_qeout_relax.py out_relax". It will generate/print on screen \*.xsf file for visulization in VESTA.

:pushpin: compare and plot the total energes of the the relaxation as a function of relaxation steps.

## "vc-RELAX" calculation (vc-relax)

Using the planewave method to relax the cell is a bit tricky. This is because as the volume of the unit cell is changing during the relaxation, the number of $`\mathbf{G}`$-vector could be changed which affects the force or stress calculation (stress will be used for cell relaxation). One possible way to cure this is to setup large enough $`E_{\rm cut}`$ (a bit larger than the converged $`E_{\rm cut}`$). This is because the stress calculation may require a higher $`E_{\rm cut}`$ to converge. e.g. for $`E_{\rm cut}`$=50 Ryd to converge the SCF total energy, 70 Rdy $`E_{\rm cut}`$ my be needed to relax the cell parameters. 

IN QE, when doing vc-relax, change calculation flag to *calculation = 'vc-relax'*, also a new namelist `&CELL /` should be added similar to `&IONS /`. Now, for this new system, the psp of each atom (Ti and O) should be a different file than Si.SG15.PBE.UPF, since the latter is used for silicon atom only. So remember to change the `ATOMIC_SPECIES` block by removing `Si xxx` and adding `Ti mass_of_Ti  filename_of_Ti_psp; O mass_of_O  filename_of_O_psp` (split into two lines).

:pushpin: Load the attached TiO2.cif file to VESTA to visualize and find out the input structure. Similar to Si case, compose the input file to relax the cell using QE. Converge the cell stress until all the components are below 0.5 kbar. Compare the relaxed cell lattice vectors with the experimental values (i.e. cell lattice length a1, a2, and a3).


## Bulk modulus calculation

:pushpin: Find out the atomic structure and the lattice vectors for **cubic diamond**. Fully relax all the atoms and the lattice vectors to get the equilibrium lattice and atomic positions. Now let's try to compute the bulk modulus for diamond. Sample several volumes from -2% to 2% of the equilibrium volume. This can be done by multiply a factor to the lattice vectors. For example, lattice vector of silicon is a 3$`\times`$3 matrix:

      0.00000000     2.71535000     2.71535000
      2.71535000     0.00000000     2.71535000
      2.71535000     2.71535000     0.00000000

Now, multiply this matrix by a factor of 0.99, we will obtain a new volume which is 0.99$`^3`$ of the original volume ($`V_0`$). Do this kind of operation for diamond. Diamond lattice could be easier since we work on its cubic form. For each volume, relax the atom **only**. Record the hydrostatic pressure (P) and total energy for each sampled volume (actually you can also find out the volume at the header of each QE calculation). The pressure could be found at the end of the calculaton such as:


    ...
      Computing stress (Cartesian axis) and pressure
 
           total   stress  (Ry/bohr**3)                   (kbar)     P=        0.01
    0.00000008   0.00000000   0.00000000            0.01        0.00        0.00
    0.00000000   0.00000008   0.00000000            0.00        0.01        0.00
    0.00000000   0.00000000   0.00000008            0.00        0.00        0.01
    ...

The average of the diagonal term will be the hydrostatic pressure (which is also the number after `P=  `.

Based on the volume dependent total energy and pressure, there are two ways to fit the pressure-volume data to get bulk modulus:

1. Simply use $`B_0 = -V\frac{\partial P}{\partial V}`$

Basically by fitting hydrostatic pressure to volume as a straight line, the slope will be proportional to the bulk modulus.

2. Use the Birch-Murnaghan isothermal EOS:

```math

E(V) = E_0 - \frac{9}{16}B_0\left[(4-B'_0)\frac{V_0^3}{V^2}-(14-3B'_0)\frac{V_0^{7/3}}{V^{4/3}}+(16-3B'_0)\frac{V_0^{5/3}}{V^{2/3}}\right]
```

Here, $`B'_0`$ is the derivative of bulk modulus pressure. It is also a constant which can be fitted here. From this equation, by fitting the total energy with respect to the volume, we can obtain equilibrium volume (which should be very close to vc-relax obtained volume), bulk modulus, derivative of bulk modulus, etc.

:pushpin: Compare the bulk modulus ($`B_0`$) by these two fitting methods and by the experimental measurements (cite the reference you find). 

:pushpin: Also calculate the bulk modulus of NaX (X=F, Cl, Br, I) with the rock salt structure (use both fitting methods) and compare with the expriments to see if they follow the same trend. You can use the cubic lattice for simplicity.
